namespace Base2art.Razor.Api
{
    public partial class TemplateRoot
    {
        private struct AttributeInfo
        {
            public int AttributeValuesCount { get; }

            public string Name { get; }

            public string Prefix { get; }

            public int PrefixOffset { get; }

            public string Suffix { get; }

            public int SuffixOffset { get; }

            public bool Suppressed { get; set; }

            public AttributeInfo(string name, string prefix, int prefixOffset, string suffix, int suffixOffset, int attributeValuesCount)
            {
                this.Name = name;
                this.Prefix = prefix;
                this.PrefixOffset = prefixOffset;
                this.Suffix = suffix;
                this.SuffixOffset = suffixOffset;
                this.AttributeValuesCount = attributeValuesCount;
                this.Suppressed = false;
            }
        }
    }
}