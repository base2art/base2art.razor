namespace Base2art.Razor.Api
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Text;

    public class ExecuteContext
    {
        private readonly Stack<ISet<string>> _currentSectionStack = new Stack<ISet<string>>();

        private ISet<string> _currentSections = new HashSet<string>();

        private readonly IDictionary<string, Stack<Action<TextWriter>>> _definedSections = new Dictionary<string, Stack<Action<TextWriter>>>();

        private readonly Stack<TemplateWriter> _bodyWriters = new Stack<TemplateWriter>();

        internal TextWriter CurrentWriter { get; set; }

        public ExecuteContext()
        {
            this._currentSectionStack.Push(new HashSet<string>());
        }

        public void DefineSection(string name, Action<TextWriter> action)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("A name is required to define a section.");
            }

            if (this._currentSections.Contains(name))
            {
                throw new ArgumentException("A section has already been defined with name '" + name + "'");
            }

            this._currentSections.Add(name);
            Stack<Action<TextWriter>> stack = default(Stack<Action<TextWriter>>);
            if (!this._definedSections.TryGetValue(name, out stack))
            {
                stack = new Stack<Action<TextWriter>>();
                this._definedSections.Add(name, stack);
            }

            stack.Push(action);
        }

        public Action<TextWriter> GetSectionDelegate(string name)
        {
            if (this._definedSections.ContainsKey(name) && this._definedSections[name].Count > 0)
            {
                return this._definedSections[name].Peek();
            }

            return null;
        }

        internal void PopSections(Action<TextWriter> inner, TextWriter innerArg)
        {
            ISet<string> currentSections = this._currentSections;
            this._currentSections = this._currentSectionStack.Pop();
            List<Tuple<string, Action<TextWriter>>> list = new List<Tuple<string, Action<TextWriter>>>();
            foreach (string currentSection in this._currentSections)
            {
                Action<TextWriter> item = this._definedSections[currentSection].Pop();
                list.Add(Tuple.Create(currentSection, item));
            }

            inner(innerArg);
            foreach (Tuple<string, Action<TextWriter>> item2 in list)
            {
                this._definedSections[item2.Item1].Push(item2.Item2);
            }

            this._currentSectionStack.Push(this._currentSections);
            this._currentSections = currentSections;
        }

        internal void PushSections()
        {
            this._currentSectionStack.Push(this._currentSections);
            this._currentSections = new HashSet<string>();
        }

        internal TemplateWriter PopBody()
        {
            return this._bodyWriters.Pop();
        }

        internal void PushBody(TemplateWriter bodyWriter)
        {
            if (bodyWriter == null)
            {
                throw new ArgumentNullException("bodyWriter");
            }

            this._bodyWriters.Push(bodyWriter);
        }
    }
}