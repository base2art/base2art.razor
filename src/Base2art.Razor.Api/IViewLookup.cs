namespace Base2art.Razor.Api
{
    using System.Collections.Generic;

    public interface IViewLookup
    {
        IReadOnlyDictionary<string, System.Type> Lookup { get; }
    }
}