namespace Base2art.Razor.Api
{
    using System;
    using System.IO;
    using Text;

    public partial class TemplateRoot
    {
        private AttributeInfo _attributeInfo;

        public virtual void WriteLiteral(string literal)
        {
            this.WriteLiteralTo(this._context.CurrentWriter, literal);
        }

        public virtual void WriteLiteral(object literal)
        {
            this.WriteLiteralTo(this._context.CurrentWriter, literal);
        }

        public virtual void WriteLiteralTo(TextWriter writer, string literal)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            if (literal != null)
            {
                writer.Write(literal);
            }
        }

        public virtual void WriteLiteralTo(TextWriter writer, object literal)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            if (literal != null)
            {
                this.WriteLiteralTo(writer, literal.ToString());
            }
        }

        public virtual void Write(object value)
        {
            this.WriteTo(this._context.CurrentWriter, value);
        }

        public virtual void WriteTo(TextWriter writer, object value)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            if (value != null)
            {
                if (value is IEncodedString es)
                {
                    writer.Write(es.ToEncodedString());
                }
                else
                {
                    IEncodedString value2 = this.Html.Encode(value);
                    writer.Write(value2);
                }
            }
        }

        public virtual void BeginWriteAttribute(string name, string prefix, int prefixOffset, string suffix, int suffixOffset,
                                                int attributeValuesCount)
        {
            this.BeginWriteAttributeTo(this._context.CurrentWriter, name, prefix, prefixOffset, suffix, suffixOffset, attributeValuesCount);
        }

        public virtual void BeginWriteAttributeTo(TextWriter writer, string name, string prefix, int prefixOffset, string suffix, int suffixOffset,
                                                  int attributeValuesCount)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (prefix == null)
            {
                throw new ArgumentNullException("prefix");
            }

            if (suffix == null)
            {
                throw new ArgumentNullException("suffix");
            }

            this._attributeInfo = new AttributeInfo(name, prefix, prefixOffset, suffix, suffixOffset, attributeValuesCount);
            if (attributeValuesCount != 1)
            {
                this.WritePositionTaggedLiteral(writer, prefix, prefixOffset);
            }
        }

        private void WritePositionTaggedLiteral(TextWriter writer, string value, int position)
        {
            this.WriteLiteralTo(writer, value);
        }

        public void WriteAttributeValue(string prefix, int prefixOffset, object value, int valueOffset, int valueLength, bool isLiteral)
        {
            this.WriteAttributeValueTo(this._context.CurrentWriter, prefix, prefixOffset, value, valueOffset, valueLength, isLiteral);
        }

        public void WriteAttributeValueTo(TextWriter writer, string prefix, int prefixOffset, object value, int valueOffset, int valueLength,
                                          bool isLiteral)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (this._attributeInfo.AttributeValuesCount == 1)
            {
                if (this.IsBoolFalseOrNullValue(prefix, value))
                {
                    this._attributeInfo.Suppressed = true;
                    return;
                }

                this.WritePositionTaggedLiteral(writer, this._attributeInfo.Prefix, this._attributeInfo.PrefixOffset);
                if (this.IsBoolTrueWithEmptyPrefixValue(prefix, value))
                {
                    value = this._attributeInfo.Name;
                }
            }

            if (value != null)
            {
                if (!string.IsNullOrEmpty(prefix))
                {
                    this.WritePositionTaggedLiteral(writer, prefix, prefixOffset);
                }

                this.WriteUnprefixedAttributeValueTo(writer, value, isLiteral);
            }
        }

        public virtual void EndWriteAttribute()
        {
            this.EndWriteAttributeTo(this._context.CurrentWriter);
        }

        public virtual void EndWriteAttributeTo(TextWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (!this._attributeInfo.Suppressed)
            {
                this.WritePositionTaggedLiteral(writer, this._attributeInfo.Suffix, this._attributeInfo.SuffixOffset);
            }
        }

        private void WriteUnprefixedAttributeValueTo(TextWriter writer, object value, bool isLiteral)
        {
            string text = value as string;
            if (isLiteral && text != null)
            {
                this.WriteLiteralTo(writer, text);
            }
            else if (isLiteral)
            {
                this.WriteLiteralTo(writer, value);
            }
            else if (text != null)
            {
                this.WriteTo(writer, text);
            }
            else
            {
                this.WriteTo(writer, value);
            }
        }
    }
}