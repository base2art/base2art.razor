namespace Base2art.Razor.Api.Text
{
    public class RawStringFactory : IEncodedStringFactory
    {
        public IEncodedString CreateEncodedString(string rawString)
        {
            return new RawString(rawString);
        }

        public IEncodedString CreateEncodedString(object value)
        {
            if (value == null)
            {
                return new RawString(string.Empty);
            }

            RawString htmlEncodedString = value as RawString;
            if (htmlEncodedString != null)
            {
                return htmlEncodedString;
            }

            return new RawString(value?.ToString());
        }

        public class RawString : IEncodedString
        {
            private readonly string _value;

            public RawString(string value)
            {
                this._value = value;
            }

            public string ToEncodedString()
            {
                return this._value ?? string.Empty;
            }

            public override string ToString()
            {
                return this.ToEncodedString();
            }
        }
    }
}