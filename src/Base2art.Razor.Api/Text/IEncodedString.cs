namespace Base2art.Razor.Api.Text
{
    public interface IEncodedString
    {
        string ToEncodedString();
    }
}