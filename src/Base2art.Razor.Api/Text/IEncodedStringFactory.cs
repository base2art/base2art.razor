namespace Base2art.Razor.Api.Text
{
    public interface IEncodedStringFactory
    {
        IEncodedString CreateEncodedString(string value);

        IEncodedString CreateEncodedString(object value);
    }
}