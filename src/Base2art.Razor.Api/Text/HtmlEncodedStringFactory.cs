namespace Base2art.Razor.Api.Text
{
    using System.Net;

    public class HtmlEncodedStringFactory : IEncodedStringFactory
    {
        public IEncodedString CreateEncodedString(string rawString)
        {
            return new HtmlEncodedString(rawString);
        }

        public IEncodedString CreateEncodedString(object value)
        {
            if (value == null)
            {
                return new HtmlEncodedString(string.Empty);
            }

            HtmlEncodedString htmlEncodedString = value as HtmlEncodedString;
            if (htmlEncodedString != null)
            {
                return htmlEncodedString;
            }

            return new HtmlEncodedString(value?.ToString());
        }

        private class HtmlEncodedString : IEncodedString
        {
            private readonly string _encodedString;

            public HtmlEncodedString(string value)
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this._encodedString = WebUtility.HtmlEncode(value);
                }
            }

            public string ToEncodedString()
            {
                return this._encodedString ?? string.Empty;
            }

            public override string ToString()
            {
                return this.ToEncodedString();
            }
        }
    }
}