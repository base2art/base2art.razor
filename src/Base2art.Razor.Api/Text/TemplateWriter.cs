namespace Base2art.Razor.Api.Text
{
    using System;
    using System.Globalization;
    using System.IO;

    public class TemplateWriter
    {
        private readonly Action<TextWriter> writerDelegate;

        public TemplateWriter(Action<TextWriter> writer)
            => this.writerDelegate = writer ?? throw new ArgumentNullException(nameof(writer));

        public override string ToString()
        {
            using (var stringWriter = new StringWriter(CultureInfo.InvariantCulture))
            {
                this.writerDelegate(stringWriter);
                return stringWriter.ToString();
            }
        }

        public void WriteTo(TextWriter writer)
        {
            this.writerDelegate(writer);
        }
    }
}