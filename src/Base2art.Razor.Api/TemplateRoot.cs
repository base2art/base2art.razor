namespace Base2art.Razor.Api
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Text;

    public partial class TemplateRoot
    {
        protected dynamic viewBag = null;

        private readonly HtmlHelper html = new HtmlHelper();
        protected ExecuteContext _context;

        protected IDictionary<string, object> ViewData { get; private set; } = new Dictionary<string, object>();

        public string Layout { get; set; }

        public dynamic ViewBag
        {
            get { return this.viewBag; }
        }

        protected TextWriter CurrentWriter
        {
            get { return this._context.CurrentWriter; }
        }

        public virtual Task ExecuteAsync()
        {
            return Task.FromResult(0);
        }

        protected IHtmlHelper Html
        {
            get { return this.html; }
        }
        
        
        private bool IsBoolFalseOrNullValue(string prefix, object value)
        {
            return string.IsNullOrEmpty(prefix) && (value == null || (value is bool && !(bool) value));
        }

        private bool IsBoolTrueWithEmptyPrefixValue(string prefix, object value)
        {
            return string.IsNullOrEmpty(prefix) && value is bool && (bool) value;
        }

        public async Task Run(ExecuteContext context, TextWriter reader)
        {
            this._context = context;
            StringBuilder builder = new StringBuilder();
            using (StringWriter writer = new StringWriter(builder))
            {
                this._context.CurrentWriter = writer;
                await this.ExecuteAsync();
                writer.Flush();
                this._context.CurrentWriter = null;
                if (this.Layout != null)
                {
//                    ITemplate layout = this.ResolveLayout(this.Layout);
//                    if (layout == null)
//                    {
//                        throw new ArgumentException("Template you are trying to run uses layout, but no layout found in cache or by resolver.");
//                    }

//                    TemplateWriter body = new TemplateWriter(delegate(TextWriter tw) { tw.Write(builder.ToString()); });
//                    context.PushBody(body);
//                    context.PushSections();
//                    await layout.Run(context, reader);
//                    return;
                }

                reader.Write(builder.ToString());
            }
        }
    }
}