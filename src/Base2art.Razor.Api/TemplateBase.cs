namespace Base2art.Razor.Api
{
    using System.Dynamic;
    using System.IO;
    using System.Threading.Tasks;
    using Web.Pages;

    public class TemplateBase<TModel> : TemplateRoot, IWebPage<TModel>
    {
        public TModel Model { get; set; }

        async Task<string> IWebPage<TModel>.ExecuteAsync(TModel model)
        {
            using (var stringWriter = new StringWriter())
            {
                var executeContext = new ExecuteContext();
                this.Model = (model);
                this.viewBag = new ExpandoObject();
                await this.Run(executeContext, stringWriter);

                await stringWriter.FlushAsync();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }

    public class TemplateBase : TemplateRoot, IWebPage
    {
        // THE RENDER METHOD

        async Task<string> IWebPage.ExecuteAsync()
        {
            using (var stringWriter = new StringWriter())
            {
                var executeContext = new ExecuteContext();
                this.viewBag = new ExpandoObject();
                await this.Run(executeContext, stringWriter);

                await stringWriter.FlushAsync();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}