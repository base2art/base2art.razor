namespace Base2art.Razor.Api
{
    using Text;

    internal class HtmlHelper : IHtmlHelper
    {
        private readonly RawStringFactory raw = new RawStringFactory();
        private readonly HtmlEncodedStringFactory html = new HtmlEncodedStringFactory();

        public IEncodedString Raw(object value) => this.raw.CreateEncodedString(value);

        public IEncodedString Raw(string value) => this.raw.CreateEncodedString(value);

        public IEncodedString Encode(object value) => this.html.CreateEncodedString(value);

        public IEncodedString Encode(string value) => this.html.CreateEncodedString(value);
    }
}