namespace Base2art.Razor.Api
{
    using Text;

    public interface IHtmlHelper
    {
        IEncodedString Raw(object value);
        IEncodedString Raw(string value);
        
        IEncodedString Encode(object value);
        IEncodedString Encode(string value);
    }
}