namespace Base2art.Razor.Compiler
{
    public class ClassIdentifier
    {
        public ClassIdentifier(string @namespace, string className)
        {
            this.Ns = @namespace;
            this.ClassName = className;
        }

        public string Ns { get; }

        public string ClassName { get; }
    }
}