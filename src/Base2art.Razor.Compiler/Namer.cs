namespace Base2art.Razor.Compiler
{
    using System;
    using System.CodeDom;
    using System.CodeDom.Compiler;
    using System.IO;
    using Microsoft.AspNetCore.Razor.Language;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Microsoft.CSharp;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public static class Namer
    {
        public static string GetDerivedNamespace(this RazorCodeDocument codeDocument, string baseNamespace)
        {
            var appendedNamespace = codeDocument.Source.RelativePath.PathPartsJoined('.', 1);
            return string.IsNullOrWhiteSpace(appendedNamespace)
                       ? baseNamespace
                       : $"{baseNamespace}.{appendedNamespace}";
        }

        public static string PrintCSharp(this Type type)
        {
            CodeDomProvider cpd = new CSharpCodeProvider();

            var options = new CodeGeneratorOptions();

            var typeReferenceExpression = new CodeTypeReferenceExpression(new CodeTypeReference(type));
            using (var writer = new StringWriter())
            {
                cpd.GenerateCodeFromExpression(typeReferenceExpression, writer, options);
                writer.Flush();
                return writer.GetStringBuilder().ToString();
            }
        }
    }
}