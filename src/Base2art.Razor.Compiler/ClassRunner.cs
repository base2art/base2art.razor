namespace Base2art.Razor.Compiler
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Razor.Language;
    using Microsoft.AspNetCore.Razor.Language.Intermediate;

    public class ClassRunner : IntermediateNodePassBase, IRazorDirectiveClassifierPass, IRazorEngineFeature, IRazorFeature
    {

        public ClassRunner()
        {
        }

        protected override void ExecuteCore(RazorCodeDocument codeDocument, DocumentIntermediateNode documentNode)
        {
            Visitor visitor = new Visitor();
            visitor.Visit(documentNode);

            foreach (var @class in visitor.Classes)
            {
                @class.Modifiers.Add("partial");
            }
        }

        private class Visitor : IntermediateNodeWalker
        {
            public List<ClassDeclarationIntermediateNode> Classes { get; } = new List<ClassDeclarationIntermediateNode>();

            public override void VisitClassDeclaration(ClassDeclarationIntermediateNode node)
            {
                base.VisitClassDeclaration(node);
                this.Classes.Add(node);
            }
        }
    }
}