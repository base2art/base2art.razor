namespace Base2art.Razor.Compiler
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Api;
    using Microsoft.AspNetCore.Mvc.Razor.Extensions;
    using Microsoft.AspNetCore.Razor.Language;
    using Microsoft.CodeAnalysis;

    public abstract class CompilerBase
    {
        protected DirectoryInfo BasePath { get; }

        protected string BaseNamespace { get; }

//        protected string Folder { get; }
        protected RazorProjectFileSystem ProjectFileSystem { get; }

        protected RazorTemplateEngine TemplateEngine { get; }
        protected Dictionary<string, ClassIdentifier> Map { get; } = new Dictionary<string, ClassIdentifier>();

        public CompilerBase(DirectoryInfo basePath, string baseNamespace)
        {
            this.BasePath = basePath;
            this.BaseNamespace = baseNamespace;
//            this.Folder = folder;
            string str = "\n@using System\n@using System.Linq\n@using System.Threading.Tasks\n";

            str = str + string.Join("\r\n", from n in new string[0] select "@using " + n.Trim()) + "\r\n";

            this.ProjectFileSystem = RazorProjectFileSystem.Create(basePath.FullName);

            var engine = RazorProjectEngine.Create(
                                                   RazorConfiguration.Default,
                                                   this.ProjectFileSystem,
                                                   builder =>
                                                   {
                                                       builder.SetBaseType("Base2art.Razor.Api.TemplateBase")
                                                              .ConfigureClass((document, @class) =>
                                                              {
                                                                  var fullPath = document.Source.RelativePath;
                                                                  var projectItem = this.ProjectFileSystem.GetItem(fullPath);

                                                                  var relativePath = projectItem.RelativePhysicalPath;
                                                                  @class.ClassName = relativePath.PathParts().Last();

                                                                  var modelName =
                                                                      ModelDirective.GetModelType(@document.GetDocumentIntermediateNode());
                                                                  var typeOf = modelName == "dynamic"
                                                                                   ? typeof(TemplateBase)
                                                                                   : typeof(TemplateBase<object>);

                                                                  var name = typeOf.PrintCSharp().Replace("object", modelName);
                                                                  //Namer.GenericNameOf(new[] {"Base2art", "Razor", "Api"},
                                                                  //                  "TemplateBase", modelName);

                                                                  @class.BaseType = name;
                                                                  this.Map[fullPath] =
                                                                      new ClassIdentifier(document.GetDerivedNamespace(this.BaseNamespace),
                                                                                          @class.ClassName);
                                                              });

                                                       builder.Features.Add(new SuppressChecksumOptionsFeature());
                                                       builder.Features.Add(new NamespaceRunner(baseNamespace));
                                                       builder.Features.Add(new ClassRunner());

                                                       ModelDirective.Register(builder);
                                                   });

            this.TemplateEngine = new RazorTemplateEngine(engine.Engine, this.ProjectFileSystem);

            this.TemplateEngine.Options.DefaultImports = RazorSourceDocument.Create(str, (string) null);
        }

        public abstract IEnumerable<string> Compile(List<SyntaxTree> syntaxTrees);

        public string EnumerationDir => this.BasePath.FullName;
    }
}