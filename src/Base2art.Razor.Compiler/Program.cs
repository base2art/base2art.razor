namespace Base2art.Razor.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Microsoft.CodeAnalysis.Text;
    using ArgumentSyntax = CommandLine.ArgumentSyntax;

    public static class Program
    {
        public static void Main(string[] args)
        {
            IReadOnlyList<string> compile = Array.Empty<string>();
            IReadOnlyList<string> refs = Array.Empty<string>();
            IReadOnlyList<string> preprocessorSymbols = Array.Empty<string>();
            IReadOnlyList<string> generatorSearchPaths = Array.Empty<string>();
            string generatedCompileItemFile = null;
            string outputDirectory = null;
            string projectDir = null;
            bool version = false;
            ArgumentSyntax.Parse(args, syntax =>
            {
                syntax.DefineOption("version", ref version, "Show version of this tool (and exits).");
                syntax.DefineOptionList("r|reference", ref refs, "Paths to assemblies being referenced");
                syntax.DefineOptionList("d|define", ref preprocessorSymbols, "Preprocessor symbols");
                syntax.DefineOptionList("generatorSearchPath", ref generatorSearchPaths, "Paths to folders that may contain generator assemblies");
                syntax.DefineOption("out", ref outputDirectory, true, "The directory to write generated source files to");
                syntax.DefineOption("projectDir", ref projectDir, true, "The absolute path of the directory where the project file is located");
                syntax.DefineOption("generatedFilesList", ref generatedCompileItemFile,
                                    "The path to the file to create with a list of generated source files");
                syntax.DefineParameterList("compile", ref compile, "Source files included in compilation");
            });

            AppDomain.CurrentDomain.AssemblyResolve += (sender, args1) =>
            {
                var objExecutingAssemblies = Assembly.GetExecutingAssembly();
                AssemblyName[] arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

                var requestedAssembly = args1.Name.Split(",")[0].Trim();

                if (string.Equals(requestedAssembly, "System.CodeDom", StringComparison.OrdinalIgnoreCase))
                {
                    var userProfile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    var systemCodeDom = Path.Combine(
                                                     userProfile,
                                                     ".nuget",
                                                     "packages",
                                                     "system.codedom",
                                                     "4.5.0",
                                                     "lib",
                                                     "netstandard2.0",
                                                     "System.CodeDom.dll");

                    var myAssembly = Assembly.LoadFile(systemCodeDom);

                    //Return the loaded assembly.
                    return myAssembly;
                }

                //Load the assembly from the specified path.
                return null;
            };
            
            var path = new DirectoryInfo(projectDir);

            var compilation = CSharpCompilation.Create("codegen")
                                               .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            var @namespace = path.Name;
            var compiler = new ProjectCompiler(path, @namespace, compilation, outputDirectory);

            var syntaxTrees = new List<SyntaxTree>();
            var output = compiler.Compile(syntaxTrees);

            var genPath = Path.Combine(
                                       path.FullName,
                                       generatedCompileItemFile);

            File.WriteAllLines(genPath, output);
        }
    }
}