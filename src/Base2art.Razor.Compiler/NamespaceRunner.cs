namespace Base2art.Razor.Compiler
{
    using Microsoft.AspNetCore.Mvc.Razor.Extensions;
    using Microsoft.AspNetCore.Razor.Language;
    using Microsoft.AspNetCore.Razor.Language.Intermediate;

    public class NamespaceRunner : IntermediateNodePassBase, IRazorDirectiveClassifierPass, IRazorEngineFeature, IRazorFeature
    {
        private readonly string baseNamespace;

        public NamespaceRunner(string baseNamespace)
        {
            this.baseNamespace = baseNamespace;
        }

        protected override void ExecuteCore(RazorCodeDocument codeDocument, DocumentIntermediateNode documentNode)
        {
            Visitor visitor = new Visitor();
            visitor.Visit(documentNode);

            NamespaceDeclarationIntermediateNode firstNamespace = visitor.FirstNamespace;
            if (firstNamespace != null)
            {
                var newNamespace = codeDocument.GetDerivedNamespace(this.baseNamespace);
                firstNamespace.Content = newNamespace;
            }
        }

        private class Visitor : IntermediateNodeWalker
        {
            public NamespaceDeclarationIntermediateNode FirstNamespace { get; private set; }

            public override void VisitNamespaceDeclaration(NamespaceDeclarationIntermediateNode node)
            {
                if (this.FirstNamespace == null)
                {
                    this.FirstNamespace = node;
                }

                base.VisitNamespaceDeclaration(node);
            }
        }
    }
}