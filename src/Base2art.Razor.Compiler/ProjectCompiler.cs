namespace Base2art.Razor.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Razor.Language;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;

    public class ProjectCompiler : CompilerBase
    {
        private const int ProcessCannotAccessFileHR = unchecked((int) 0x80070020);
        private readonly CSharpCompilation options;
        private string intermediateOutputDirectory;

        public ProjectCompiler(
            DirectoryInfo basePath,
            string baseNamespace,
            CSharpCompilation options,
            string intermediateOutputDirectory) : base(basePath, baseNamespace)
        {
            this.options = options;
            this.intermediateOutputDirectory = intermediateOutputDirectory;
        }

        public static readonly DiagnosticDescriptor Descriptor = new DiagnosticDescriptor(
                                                                                          "B2ASER1",
                                                                                          "MayNeedToClean",
                                                                                          "You may need to run a dotnet/msbuild clean or build again "
                                                                                          + "to get a successful build.",
                                                                                          "Category1",
                                                                                          defaultSeverity: DiagnosticSeverity.Warning,
                                                                                          isEnabledByDefault: true,
                                                                                          description: "Rebuild may be needed",
                                                                                          helpLinkUri: "https://not-relevant.com",
                                                                                          customTags: new string [0]);

        public override IEnumerable<string> Compile(List<SyntaxTree> syntaxTrees)
        {
            List<string> outputPaths = new List<string>();
            var compOptions = new CSharpParseOptions();
            foreach (var file in this.ProjectFileSystem.EnumerateItems(this.EnumerationDir))
            {
                FileSystemRazorProjectItemWrapper projectItem2 = new FileSystemRazorProjectItemWrapper(file);

                RazorCSharpDocument razorCSharpDocument = this.TemplateEngine.GenerateCode(projectItem2);

                var syntaxTree = CSharpSyntaxTree.ParseText(razorCSharpDocument.GeneratedCode, compOptions, file.PhysicalPath);
                syntaxTrees.Add(syntaxTree);
                this.options.AddSyntaxTrees(syntaxTree);
            }

            var viewLookupWrapper = ViewLookup.Generate(this.BaseNamespace, this.Map);

            var viewLookup = CSharpSyntaxTree.ParseText(
                                                        viewLookupWrapper.GetText(),
                                                        compOptions,
                                                        Path.Combine(this.BasePath.FullName, "ViewLookup.cs"));
//            outputPaths.Add("ViewLookup.cs");

            syntaxTrees.Add(viewLookup);
            options.AddSyntaxTrees(viewLookup);

            ////////////////////////////////////

            var cancellationToken = CancellationToken.None;
            using (var hasher = System.Security.Cryptography.SHA1.Create())
            {
                bool writeOther = false;
                foreach (var inputSyntaxTree in syntaxTrees.Where(x => !string.IsNullOrWhiteSpace(x.FilePath)))
                {
                    if (this.WriteFileContent(hasher, inputSyntaxTree, x => File.GetLastWriteTime(x.FilePath), cancellationToken, outputPaths))
                    {
                        writeOther = true;
                    }
                }

                if (writeOther)
                {
                    foreach (var inputSyntaxTree in syntaxTrees.Where(x => string.IsNullOrWhiteSpace(x.FilePath)))
                    {
                        this.WriteFileContent(hasher, inputSyntaxTree, x => DateTime.MaxValue, cancellationToken, outputPaths);
                    }
                }
            }

            return outputPaths;
        }

        private bool WriteFileContent(
            SHA1 hasher,
            SyntaxTree inputSyntaxTree,
            Func<SyntaxTree, DateTime> lastModifiedTime,
            CancellationToken cancellationToken,
            List<string> outputPaths)
        {
            cancellationToken.ThrowIfCancellationRequested();

            string sourceHash = Convert.ToBase64String(hasher.ComputeHash(Encoding.UTF8.GetBytes(inputSyntaxTree.FilePath)), 0, 6)
                                       .Replace('/', '-');
            Logger.Info($"File \"{inputSyntaxTree.FilePath}\" hashed to {sourceHash}");

            string normalOutputFilePath = Path.Combine(inputSyntaxTree.FilePath + ".generated.cs");

            string outputFilePath = Path.Combine(this.IntermediateOutputDirectory,
                                                 Path.GetFileNameWithoutExtension(inputSyntaxTree.FilePath) + $".{sourceHash}.generated.cs");

            outputPaths.Add(outputFilePath);

            // Code generation is relatively fast, but it's not free.
            // So skip files that haven't changed since we last generated them.
            DateTime outputLastModified = File.Exists(outputFilePath) ? File.GetLastWriteTime(outputFilePath) : DateTime.MinValue;

            // File.GetLastWriteTime(inputSyntaxTree.FilePath)
            //  || assembliesLastModified > outputLastModified
            if (lastModifiedTime(inputSyntaxTree) > outputLastModified || IsDebug)
            {
                int retriesLeft = 3;
                do
                {
                    try
                    {
                        var outputText = inputSyntaxTree.GetText(cancellationToken);

                        using (var outputFileStream = File.OpenWrite(outputFilePath))
                        {
                            using (var outputWriter = new StreamWriter(outputFileStream))
                            {
                                outputText.Write(outputWriter);

                                // Truncate any data that may be beyond this point if the file existed previously.
                                outputWriter.Flush();
                                outputFileStream.SetLength(outputFileStream.Position);
                            }
                        }

                        var classVirtualizationVisitor = new ClassVirtualizationVisitor();
                        var syntaxNode = inputSyntaxTree.GetRoot();
                        var output = classVirtualizationVisitor.Visit(syntaxNode);

//                        inputSyntaxTree.GetRoot().

//                        var newSyntaxTree = classVirtualizationVisitor.FirstNamespace;
                        outputText = output.GetText();
                        using (var outputFileStream = File.OpenWrite(normalOutputFilePath))
                        {
                            using (var outputWriter = new StreamWriter(outputFileStream))
                            {
                                outputText.Write(outputWriter);

                                // Truncate any data that may be beyond this point if the file existed previously.
                                outputWriter.Flush();
                                outputFileStream.SetLength(outputFileStream.Position);
                            }
                        }

                        return true;
                    }
                    catch (IOException ex) when (ex.HResult == ProcessCannotAccessFileHR && retriesLeft > 0)
                    {
                        retriesLeft--;
                        Task.Delay(200).Wait();
                    }
                    catch (Exception)
                    {
//                                ReportError(progress, "CGR001", inputSyntaxTree, ex);
//                                fileFailures.Add(ex);
                        return false;
                    }
                }
                while (true);
            }

            return false;
//                    this.generatedFiles.Add(outputFilePath);
        }

        public bool IsDebug => true;

        public string IntermediateOutputDirectory => this.intermediateOutputDirectory;

        private class ClassVirtualizationVisitor : CSharpSyntaxRewriter
        {
            public override SyntaxNode VisitNamespaceDeclaration(NamespaceDeclarationSyntax node)
            {
                node = node.NormalizeWhitespace();

//                           Trivia(
                node = node.WithTrailingTrivia(new[] {SyntaxFactory.LineFeed}.Concat(node.GetTrailingTrivia()));
                var result = base.VisitNamespaceDeclaration(node);

//                result.withTr
                return result;
//                return result.NormalizeWhitespace();
            }

//            public override SyntaxNode VisitClassDeclaration(ClassDeclarationSyntax node)
//            {
//                var x = (ClassDeclarationSyntax) base.VisitClassDeclaration(node);
//
//                return x.WithMembers(new SyntaxList<MemberDeclarationSyntax>(node.Members
//                                                                                 .Select(this.Transform)
//                                                                                 .Where(y => y != null)));
//            }

            public override SyntaxNode VisitClassDeclaration(ClassDeclarationSyntax node)
            {
                var x = (ClassDeclarationSyntax) base.VisitClassDeclaration(node);

                x = x.WithMembers(new SyntaxList<MemberDeclarationSyntax>(node.Members
                                                                              .Select(this.Transform)
                                                                              .Where(y => y != null)));

                return x.WithCloseBraceToken(x.CloseBraceToken.WithLeadingTrivia(new List<SyntaxTrivia>()))
                        .NormalizeWhitespace();
            }

            private MemberDeclarationSyntax Transform(MemberDeclarationSyntax arg)
            {
                if (arg is MethodDeclarationSyntax
                    || arg is FieldDeclarationSyntax
                    || arg is PropertyDeclarationSyntax
                    || arg is ConstructorDeclarationSyntax)
                {
                    return this.GetTrivia(arg);
                }

                return arg;
            }

            private MemberDeclarationSyntax GetTrivia(MemberDeclarationSyntax p0)
            {
                return null;
            }

//            public NamespaceDeclarationSyntax FirstNamespace { get; set; }
        }
    }
}