namespace Base2art.Razor.Compiler
{
    using System.Collections.Generic;
    using System.Linq;
    using Api;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class ViewLookup
    {
        public static CompilationUnitSyntax Generate(string @namespace, Dictionary<string, ClassIdentifier> paths)
        {
            return CompilationUnit()
                   .WithMembers(
                                SingletonList<MemberDeclarationSyntax>(
                                                                       NamespaceDeclaration(IdentifierName(@namespace))
                                                                           .WithUsings(
                                                                                       SingletonList(
                                                                                                     UsingDirective(
                                                                                                                    QualifiedName(
                                                                                                                                  QualifiedName(
                                                                                                                                                IdentifierName("System"),
                                                                                                                                                IdentifierName("Collections")),
                                                                                                                                  IdentifierName("Generic")))))
                                                                           .WithMembers(
                                                                                        SingletonList<MemberDeclarationSyntax>(ClassDec(paths)))))
                   .NormalizeWhitespace();
        }

        private static ClassDeclarationSyntax ClassDec(Dictionary<string, ClassIdentifier> paths)
        {
            return ClassDeclaration("ViewLookup")
                   .WithBaseList(BaseList(
                                          SingletonSeparatedList<BaseTypeSyntax>(
                                                                                 SimpleBaseType(IdentifierName(typeof(IViewLookup).PrintCSharp())))))
                   .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword), Token(SyntaxKind.PartialKeyword)))
                   .WithMembers(
                                List(
                                     new MemberDeclarationSyntax
                                     []
                                     {
                                         FieldDeclaration(DictVariable())
                                             .WithModifiers(TokenList(Token(SyntaxKind.PrivateKeyword), Token(SyntaxKind.ReadOnlyKeyword))),
                                         ConstructorDeclaration(Identifier("ViewLookup"))
                                             .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword)))
                                             .WithBody(Block(GetAssignmentExpression(paths))),
                                         Property()
                                     }));
        }

        private static MemberDeclarationSyntax Property()
        {
            return PropertyDeclaration(
                                       GenericName(
                                                   Identifier("IReadOnlyDictionary"))
                                           .WithTypeArgumentList(
                                                                 TypeArgumentList(
                                                                                  SeparatedList<TypeSyntax>(
                                                                                                            new SyntaxNodeOrToken[]
                                                                                                            {
                                                                                                                PredefinedType(
                                                                                                                               Token(SyntaxKind
                                                                                                                                         .StringKeyword)),
                                                                                                                Token(SyntaxKind.CommaToken),
                                                                                                                IdentifierName("System.Type")
                                                                                                            }))),
                                       Identifier("Lookup"))
                   .WithModifiers(
                                  TokenList(
                                            Token(SyntaxKind.PublicKeyword)))
                   .WithExpressionBody(
                                       ArrowExpressionClause(
                                                             MemberAccessExpression(
                                                                                    SyntaxKind.SimpleMemberAccessExpression,
                                                                                    ThisExpression(),
                                                                                    IdentifierName("lookup"))))
                   .WithSemicolonToken(
                                       Token(SyntaxKind.SemicolonToken));
        }

        private static VariableDeclarationSyntax DictVariable()
        {
            var typeArgumentListSyntax = TypeArgumentList(
                                                          SeparatedList<TypeSyntax>(
                                                                                    new SyntaxNodeOrToken
                                                                                    []
                                                                                    {
                                                                                        PredefinedType(Token(SyntaxKind.StringKeyword)),
                                                                                        Token(SyntaxKind.CommaToken),
                                                                                        IdentifierName("System.Type")
                                                                                    }));
            return VariableDeclaration(
                                       GenericName(Identifier("Dictionary"))
                                           .WithTypeArgumentList(typeArgumentListSyntax))
                .WithVariables(
                               SingletonSeparatedList(
                                                      VariableDeclarator(Identifier("lookup"))
                                                          .WithInitializer(
                                                                           EqualsValueClause(
                                                                                             ObjectCreationExpression(
                                                                                                                      GenericName(Identifier("Dictionary"))
                                                                                                                          .WithTypeArgumentList(typeArgumentListSyntax))
                                                                                                 .WithArgumentList(ArgumentList())))));
        }

        private static StatementSyntax[] GetAssignmentExpression(Dictionary<string, ClassIdentifier> dictionary)
        {
            return dictionary.Select(x =>
                             {
                                 var key = LiteralExpression(
                                                             SyntaxKind.StringLiteralExpression,
                                                             Literal(x.Key));
                                 var value = TypeOfExpression(
                                                              IdentifierName(string.Concat(x.Value.Ns, ".", x.Value.ClassName)));
                                 return ExpressionStatement(
                                                            AssignmentExpression(
                                                                                 SyntaxKind.SimpleAssignmentExpression,
                                                                                 ElementAccessExpression(IdentifierName("lookup"))
                                                                                     .WithArgumentList(
                                                                                                       BracketedArgumentList(
                                                                                                                             SingletonSeparatedList(Argument(key)))),
                                                                                 value));
                             })
                             .Select<ExpressionStatementSyntax, StatementSyntax>(x => x)
                             .ToArray();
        }
    }
}