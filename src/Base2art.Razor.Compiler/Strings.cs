namespace Base2art.Razor.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class Strings
    {
        public static string PathPartsJoined(this string relativePath, char sep, int skipCount)
        {
            var parts2 = PathParts(relativePath);
            return string.Join(new string(new char[] {sep}), parts2.Reverse().Skip(skipCount).Reverse());
        }

        public static IEnumerable<string> PathParts(this string relativePath)
        {
            List<string> pathParts = new List<string>();

            var folder = Path.GetDirectoryName(relativePath);
            var file = Path.GetFileName(relativePath);
            while (!string.IsNullOrWhiteSpace(folder))
            {
                pathParts.Insert(0, Path.GetFileName(folder));
                folder = Path.GetDirectoryName(folder);
            }

            if (file.EndsWith(".cshtml", StringComparison.OrdinalIgnoreCase))
            {
                pathParts.Add(file.Substring(0, file.Length - ".cshtml".Length));
            }

            IEnumerable<string> parts2 = pathParts;
            return parts2;
        }
    }
}