namespace Base2art.Razor.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Microsoft.AspNetCore.Razor.Language;
    using Microsoft.CodeAnalysis;

    public class FileSystemCompiler : CompilerBase
    {
        public FileSystemCompiler(DirectoryInfo basePath, string baseNamespace) : base(basePath, baseNamespace)
        {
        }

        public override IEnumerable<string> Compile(List<SyntaxTree> syntaxTrees)
        {
            var result = new List<string>();

            foreach (var file in this.ProjectFileSystem.EnumerateItems(EnumerationDir))
            {
                FileSystemRazorProjectItemWrapper projectItem2 = new FileSystemRazorProjectItemWrapper(file);

                RazorCSharpDocument razorCSharpDocument = this.TemplateEngine.GenerateCode(projectItem2);
                var cshtmlPath = Path.ChangeExtension(file.PhysicalPath, ".cs");
                File.WriteAllText(cshtmlPath, razorCSharpDocument.GeneratedCode);

                result.Add(cshtmlPath);
            }

            var viewLookup = ViewLookup.Generate(this.BaseNamespace, this.Map);
            var lookupPath = Path.Combine(this.BasePath.FullName, "ViewLookup.cs");
            File.WriteAllText(lookupPath, viewLookup.ToFullString());
            result.Add(lookupPath);

            return result;
        }

        
    }
}