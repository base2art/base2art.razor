namespace Base2art.Razor.Compiler
{
    using System;
    using System.IO;
    using Microsoft.AspNetCore.Razor.Language;

    public class FileSystemRazorProjectItemWrapper : RazorProjectItem
    {
        private readonly RazorProjectItem _source;

        public override string BasePath
        {
            get { return this._source.BasePath; }
        }

        public override string RelativePhysicalPath => this._source.RelativePhysicalPath;

        public override string FilePath
        {
            get { return this._source.FilePath; }
        }

        public override string PhysicalPath
        {
            get { return this._source.PhysicalPath; }
        }

        public override bool Exists
        {
            get { return this._source.Exists; }
        }

        public FileSystemRazorProjectItemWrapper(RazorProjectItem item)
        {
            this._source = item;
        }

        public override Stream Read()
        {
            string s = this.ProcessFileIncludes();
            return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(s));
        }

        private string ProcessFileIncludes()
        {
            string directoryName = Path.GetDirectoryName(this._source.PhysicalPath);
            string text = File.ReadAllText(this._source.PhysicalPath);
            string text2 = "<%$ include: ";
            string text3 = " %>";
            int num;
            string text5;
            for (num = 0; num < text.Length; num += text5.Length)
            {
                num = text.IndexOf(text2, num);
                if (num == -1)
                {
                    break;
                }

                int num2 = text.IndexOf(text3, num);
                if (num2 == -1)
                {
                    throw new InvalidOperationException("Invalid include file format in " + this._source.PhysicalPath
                                                                                          + ". Usage example: <%$ include: ErrorPage.js %>");
                }

                string text4 = text.Substring(num + text2.Length, num2 - (num + text2.Length));
                Console.WriteLine("      Inlining file {0}", text4);
                text5 = File.ReadAllText(Path.Combine(directoryName, text4));
                text = text.Substring(0, num) + text5 + text.Substring(num2 + text3.Length);
            }

            return text;
        }
    }
}