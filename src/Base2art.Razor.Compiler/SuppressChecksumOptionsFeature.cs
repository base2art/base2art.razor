namespace Base2art.Razor.Compiler
{
    using System;
    using Microsoft.AspNetCore.Razor.Language;

    internal class SuppressChecksumOptionsFeature : RazorEngineFeatureBase, IConfigureRazorCodeGenerationOptionsFeature, IRazorEngineFeature, IRazorFeature
    {
        public int Order { get; set; }

        public void Configure(RazorCodeGenerationOptionsBuilder options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            options.SuppressChecksum = true;
            options.SuppressMetadataAttributes = true;
        }
    }
}