namespace Base2art.Razor.Features.Integration.ViewModels.CompileNested
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;

    public class Test1ViewModels
    {
        public async Task<MethodBase> Inner()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(500));
            return MethodBase.GetCurrentMethod();
        }
    }
}