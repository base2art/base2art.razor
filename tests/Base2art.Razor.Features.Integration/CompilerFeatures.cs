﻿namespace Base2art.Razor.Features.Integration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using FluentAssertions;
    using Microsoft.CodeAnalysis;
    using Web.Pages;
    using Xunit;
    using Compiler;
    using ViewModels.CompileNested;

    public class CompilerFeatures
    {
        [Fact(Skip = "SjY skipping")]
        public void ShouldLoad()
        {
            var basePath = @"/home/tyoung/code/b2a/base2art.web.app.pages/tests/Base2art.Razor.Features.Integration";
            var path = Path.Combine(basePath, "Pages", "Test1.cs");

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            File.Exists(path)
                .Should()
                .BeFalse();

            var compiler = new FileSystemCompiler(new DirectoryInfo(basePath),
                                                  "Base2art.Razor.Features.Integration");

            compiler.Compile(new List<SyntaxTree>());

            File.Exists(path)
                .Should()
                .BeTrue();
        }

        [Fact]
        public async void ShouldLoadFromAutoCompile()
        {
            var result = await WebPage.Create(new Pages.Test1()).Render();
            result.Clean().Should().Be($@"
{DateTime.Now.Date}
This <br /> is a string.
This &lt;br /&gt; is a string.
This &lt;br /&gt; is a string.".Clean());
        }

        [Fact]
        public async void ShouldLoadFromAutoCompileAsyncMethod()
        {
            var result = await WebPage.Create(new Pages.CompileNested.Test1(), new Test1ViewModels()).Render();
            result.Clean().Should().Be($@"MoveNext".Clean());
        }
    }

    public static class Strings
    {
        public static string Clean(this string value)
        {
            value = value.Replace("\r", "\n");

            while (value.Contains("\n\n"))
            {
                value = value.Replace("\n\n", "\n");
            }

            return value.Trim();
        }
    }
}